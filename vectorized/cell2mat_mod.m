function [M] = cell2mat_mod (C, cat_dim)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      Leonardo S. R. Oliveira
%                                            16-mar-2021
%
% -- Transforma um array de celulas numa matrix numerica
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% INITIALIZE MATRIX
M = C{1}';
%
%% CONCATENATE
for i=2:length(C)
    M = cat(cat_dim, M, C{i}');
end
%
%% REARRANGE
% M = flip(M);
%