function [stencil_cells,stencil_faces,stencil_size]=Stencil(order)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                  12 de Outubro de 2016                                            %
%                                  13 de Outubro de 2016                                            %
%                                                                                                   %
% Stencil de 6ª Ordem, é necessário expandir o stencil até aos 3 vizinhos de vertice de cada face,  %
% nas fronteiras é feito uma extensão do stencil de forma a que apenas seja extendido na direcção   %
% que o stencil não está completo, neste caso em cada direcção são necessários 6 pontos             %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
global TempoGlobal fid;
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells faces;
global cell_verts cell_faces cell_vol cell_norm cell_bound cell_vert_num cell_face_num extended_stencil;
global face_vert face_cells face_area face_bound;
global vert_cells vert_cell_num vert_face_num;
global phi lap_phi  phi_faces flux_phi_faces;
%
% Determinação do Tipo de Vizinhos %
% neib=0 2ª Ordem %
% neib=1 4ª Ordem %
% neib=2 6ª Ordem %
% neib=3 8ª Ordem %
%
neib=order/2-1;
%
%% Vizinhos de Vertice - 1º Vizinhos %%
%
% Pre-alocar tamanho do stencil %
max_stencil_size = order^2 + order; % malha cartesiana
stencil_cells = spalloc(face_num, max_stencil_size, face_num*6); % x6 em cart 2D, x18 em cart 3D
stencil_size = zeros(face_num, 3); % [total, cells, faces]
%
% Adiciona as Células dos 1º e 2º Vertices da Face %
for i=1:face_num
    vizinhos_1 = union( vert_cells(face_vert(i,1),:), ...
                        vert_cells(face_vert(i,2),:)  );
    stencil_cells(i, 1:length(vizinhos_1)) = vizinhos_1';
end
stencil_size(:,2) = sum(stencil_cells ~=0, 2);
%
%% TODO
%% Restantes Vizinhos de Vertices da Face %%
for n=1:neib
    
end
%
%% TODO: qual assigment eh mais eficiente: column ou row?
%% Organizar matriz do stencil
stencil_cells = sort(stencil_cells, 2, 'descend');
%
%
%% Adicionar as Faces de Fronteira ao Stencil %%
% Variaveis auxiliares
aux_faces_all = 1:face_num;
aux_cells_all = 1:cell_num;
aux_faces_bound = aux_faces_all(face_bound(:,1)==1);
aux_cells = aux_cells_all(cell_bound(:,1)==1);
aux_verts = unique(cell_verts(aux_cells,:));
[~,aux_faces_a,~] = intersect(face_vert(1:(end/2),1), aux_verts);
[~,aux_faces_b,~] = intersect(face_vert(1:(end/2),2), aux_verts);
[~,aux_faces_c,~] = intersect(face_vert((end/2+1):end,1), aux_verts);
[~,aux_faces_d,~] = intersect(face_vert((end/2+1):end,2), aux_verts);
aux_faces = union( union(aux_faces_a, aux_faces_b), face_num/2 + union(aux_faces_c, aux_faces_d));
%
for n=1:neib
    aux_cells = setdiff( face_cells(aux_faces,:), [0 aux_cells] );
    aux_verts = unique(cell_verts(aux_cells,:));
    [~,aux_faces_a,~] = intersect(face_vert(1:(end/2),1), aux_verts);
    [~,aux_faces_b,~] = intersect(face_vert(1:(end/2),2), aux_verts);
    [~,aux_faces_c,~] = intersect(face_vert((end/2+1):end,1), aux_verts);
    [~,aux_faces_d,~] = intersect(face_vert((end/2+1):end,2), aux_verts);
    aux_faces = union( union(aux_faces_a, aux_faces_b), face_num/2 + union(aux_faces_c, aux_faces_d));
end
%
% Pre-alocar memoria
stencil_faces = spalloc(face_num, order*2+1, 4*cell_side*length(aux_faces)*(order*2+1));
% 
for i=aux_faces'
    aux_stencil_faces = intersect( cell_faces( intersect( aux_cells, stencil_cells(i,:)), :), aux_faces_bound);
    stencil_faces(i,1:length(aux_stencil_faces)) = aux_stencil_faces';
end
%
stencil_size(:,3) = sum(stencil_faces~=0, 2);
stencil_size(:,1) = stencil_size(:,2) + stencil_size(:,3);
%
%% Verificação da Necessidade de Expansao do Stencil %%
%
% Obter coordenadas das celulas/faces, e area individual das celulas
aux_x_cells(stencil_cells~=0) = cells( stencil_cells(stencil_cells~=0), 1);
aux_y_cells(stencil_cells~=0) = cells( stencil_cells(stencil_cells~=0), 2);
aux_x_faces(stencil_faces~=0) = faces( stencil_faces(stencil_faces~=0), 1);
aux_y_faces(stencil_faces~=0) = faces( stencil_faces(stencil_faces~=0), 2);
aux_area(stencil_cells~=0) = cell_vol( stencil_cells(stencil_cells~=0));
%
% Substituir zeros por -Inf (para calculo do minimo)
aux_x_cells(stencil_cells==0) = -Inf;
aux_y_cells(stencil_cells==0) = -Inf;
aux_x_faces(stencil_faces==0) = -Inf;
aux_y_faces(stencil_faces==0) = -Inf;
aux_area(stencil_cells==0) = 0;
%
% Voltar a forma original (vetor -> matriz)
aux_x_cells = reshape(aux_x_cells, size(stencil_cells));
aux_y_cells = reshape(aux_y_cells, size(stencil_cells));
aux_x_faces = reshape(aux_x_faces, size(stencil_faces));
aux_y_faces = reshape(aux_y_faces, size(stencil_faces));
aux_area = reshape(aux_area, size(stencil_cells));
%
% Calcular max/min das coordenadas e area total do stencil
aux_x_cells = [max(aux_x_cells,[],2) min(abs(aux_x_cells),[],2)];
aux_y_cells = [max(aux_y_cells,[],2) min(abs(aux_y_cells),[],2)];
aux_x_faces = [max(aux_x_faces,[],2) min(abs(aux_x_faces),[],2)];
aux_y_faces = [max(aux_y_faces,[],2) min(abs(aux_y_faces),[],2)];
aux_x = [max(aux_x_cells,aux_x_faces) min(aux_x_cells,aux_x_faces)];
aux_y = [max(aux_y_cells,aux_y_faces) min(aux_y_cells,aux_y_faces)];
aux_area = sum(aux_area, 2);
%
% Determinação do Comprimento de Referencia do Stencil %
L_stencil = sqrt(aux_area./stencil_size(:,2));
%
% Determina em que direcção o Stencil deve crescer %
L_x = ( aux_x(:,1) - aux_x(:,end) ) ./ L_stencil;
L_y = ( aux_y(:,1) - aux_y(:,end) ) ./ L_stencil;
%
% Determina quantos pontos tem o Stencil em cada direcção %
n_x = round( L_x + 1.1 );
n_y = round( L_y + 1.1 );
%
% Verificar orientacao da face (normal exterior) %
nx = verts(face_vert(:,1),1) - verts(face_vert(:,2),1);
ny = verts(face_vert(:,1),2) - verts(face_vert(:,2),2);
%
% Ajustar ordem
ordem_x = order(ones(face_num,1));
ordem_y = order(ones(face_num,1));
if extended_stencil
    ordem_x(ny==0) = order+1;
    ordem_y(nx==0) = order+1; 
end
%
%% Extender stencil %%
% Stencils que serao extendidos
aux_bool_x = n_x<ordem_x; % direcao xx
aux_bool_y = n_y<ordem_y; % direcao yy
aux_bool_or = ( aux_bool_x | aux_bool_y );
aux_bool_xor = xor( aux_bool_x, aux_bool_y );
% Vector Auxiliar com Vertices dos Stencils que serao extendidos %
% n_verts_total = sum(2+2*stencil_size(stencil_size(aux_bool,2)<4,2)) + sum(3+1.5*stencil_size(stencil_size(aux_bool,2)>=4,2)); % 2a ordem
%
while sum(aux_bool_or)
    clear aux_x_cells aux_y_cells aux_x_faces aux_y_faces aux_area

    for i=aux_faces_all(aux_bool_or)
        % Vertices das celulas do stencil i
        aux_verts = unique(cell_verts(stencil_cells(i,1:stencil_size(i,2)),:)); 
        %
        % Celulas adjacentes que nao sao parte do stencil
        aux_cells = setdiff(vert_cells(aux_verts,:), stencil_cells(i,:));
        %
        % Se expansao for restringida em xx/yy
        if aux_bool_xor(i)
            aux_cells_xx = [];
            aux_cells_yy = [];
            if aux_bool_x(i)
                aux_cells_xx = aux_cells( cells(aux_cells,2)<=aux_y(i,1) & cells(aux_cells,2)>=aux_y(i,end) );
            end
            if aux_bool_y(i)
                aux_cells_yy = aux_cells( cells(aux_cells,1)<=aux_x(i,1) & cells(aux_cells,1)>=aux_x(i,end) );
            end
            aux_cells = union( aux_cells_xx, aux_cells_yy);
        end
        %
        % Novo stencil
        aux_cells = union( aux_cells, stencil_cells(i,1:stencil_size(i,2)) );   % redundante?
        aux_faces = intersect( cell_faces(aux_cells,:), aux_faces_bound );
        stencil_cells(i,1:length(aux_cells)) = flip(aux_cells);                 % sorted
        stencil_faces(i,1:length(aux_faces)) = aux_faces;                       % sorted
    end
    %
    % Parte nova do stencil
    aux_stencil_cells = stencil_cells(aux_bool_or,:);
    aux_stencil_faces = stencil_faces(aux_bool_or,:);
    %
    % Calcular novo tamanho do stencil
    stencil_size(aux_bool_or,2) = sum(aux_stencil_cells~=0, 2);
    stencil_size(aux_bool_or,3) = sum(aux_stencil_faces~=0, 2);
    stencil_size(aux_bool_or,1) = stencil_size(aux_bool_or,2) + stencil_size(aux_bool_or,3);
    % 
    % Verificar se extensoes foram suficientes 
    % (codigo essencialmente igual ao anterior, mas considerando apenas a parte nova do stencil)
    aux_x_cells(aux_stencil_cells~=0) = cells( aux_stencil_cells(aux_stencil_cells~=0), 1);
    aux_y_cells(aux_stencil_cells~=0) = cells( aux_stencil_cells(aux_stencil_cells~=0), 2);
    aux_x_faces(aux_stencil_faces~=0) = faces( aux_stencil_faces(aux_stencil_faces~=0), 1);
    aux_y_faces(aux_stencil_faces~=0) = faces( aux_stencil_faces(aux_stencil_faces~=0), 2);
    aux_area(aux_stencil_cells~=0) = cell_vol( aux_stencil_cells(aux_stencil_cells~=0));
    aux_x_cells(aux_stencil_cells==0) = -Inf;
    aux_y_cells(aux_stencil_cells==0) = -Inf;
    aux_x_faces(aux_stencil_faces==0) = -Inf;
    aux_y_faces(aux_stencil_faces==0) = -Inf;
    aux_area(aux_stencil_cells==0) = 0;
    aux_x_cells = reshape(aux_x_cells, size(aux_stencil_cells));
    aux_y_cells = reshape(aux_y_cells, size(aux_stencil_cells));
    aux_x_faces = reshape(aux_x_faces, size(aux_stencil_faces));
    aux_y_faces = reshape(aux_y_faces, size(aux_stencil_faces));
    aux_area = reshape(aux_area, size(aux_stencil_cells));
    aux_x_cells = [max(aux_x_cells,[],2) min(abs(aux_x_cells),[],2)];
    aux_y_cells = [max(aux_y_cells,[],2) min(abs(aux_y_cells),[],2)];
    aux_x_faces = [max(aux_x_faces,[],2) min(abs(aux_x_faces),[],2)];
    aux_y_faces = [max(aux_y_faces,[],2) min(abs(aux_y_faces),[],2)];
    aux_x = [max(aux_x_cells,aux_x_faces) min(aux_x_cells,aux_x_faces)];
    aux_y = [max(aux_y_cells,aux_y_faces) min(aux_y_cells,aux_y_faces)];
    aux_area = sum(aux_area, 2);
    L_stencil = sqrt(aux_area./stencil_size(aux_bool_or,2));
    L_x = ( aux_x(:,1) - aux_x(:,end) ) ./ L_stencil;
    L_y = ( aux_y(:,1) - aux_y(:,end) ) ./ L_stencil;
    n_x(aux_bool_or) = round( L_x + 1.1 );
    n_y(aux_bool_or) = round( L_y + 1.1 );
    aux_bool_x = n_x<ordem_x;
    aux_bool_y = n_y<ordem_y;
    aux_bool_or = ( aux_bool_x | aux_bool_y );
    aux_bool_xor = xor( aux_bool_x, aux_bool_y );
end
%
stencil_faces = sort(stencil_faces, 2, 'descend');
%
