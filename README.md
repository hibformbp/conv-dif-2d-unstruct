### Description:

A Very High-Order Finite Volume Technique for Convection-Diffusion Problems on Unstructured Grids.

Original code by Filipe Diogo.
Vectorized code by Leonardo S. R. Oliveira

### References:

Diogo FJM. “A Very High-Order Finite Volume Technique for Convection-Diffusion Problems on Unstructured Grids”. MSc thesis. Instituto Superior Técnico, Universidade de Lisboa, 2019. URL: https://fenix.tecnico.ulisboa.pt/cursos/meaer/dissertacao/283828618790445
