function [norma1_phi,norma1_lap_phi,erro_max_phi,erro_max_lap_phi,erro_phi,erro_lap_phi,X]=errorcalculation(explicito)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   22 de Setembro de 2016                                          %
%                                                                                                   %
% Cálculo dos Erros Numéricos                                                                       %
%                                                                                                   %
% norma1_phi - Norma 1 do Erro da Propriedade                                                       %
% norma1_lap - Norma 1 do Erro do Laplaciano                                                        %
% erro_phi - Distribuição do Erro da Propriedade                                                    %
% erro_lap - Distribuição do Erro do Laplaciano                                                     %
% erro_phi_max - Valor do Erro Máximo da Propriedade                                                %
% erro_lap_max - Valor do Erro Máximo do Laplaciano                                                 &
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%% Declaração das Variaveis Globais %%
%
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells cell_verts cell_faces cell_vol faces face_area face_vert;
global face_bound cell_bound face_cells vert_cells;
global phi lap_phi  phi_faces flux_phi_faces;
global phi_num lap_phi_num;
%
% %% Inicializar variaveis de erro %%
% erro_phi        = zeros(cell_num,1);
% erro_lap_phi    = zeros(cell_num,1);
% %
% erro_phi_total      = 0;
% erro_lap_phi_total  = 0;
% %
% erro_max_phi        = 0;
% erro_max_lap_phi    = 0;
%
%
%% Calculo do erro %%
erro_lap_phi = abs( lap_phi - lap_phi_num );
erro_phi     = abs(     phi -     phi_num );
%
erro_lap_phi_total = sum( erro_lap_phi .* cell_vol );
erro_phi_total     = sum( erro_phi     .* cell_vol );
%
[erro_max_lap_phi, i] = max( erro_lap_phi );    % erro max do laplaciano
[erro_max_phi,     j] = max( erro_phi );        % erro max do phi
X = [cells(i,:); cells(j,:)];                   % coordenadas dos erros maximos
%
%
%% Normalizar erro %%
norma1_lap_phi = erro_lap_phi_total / L^2;
norma1_phi     = erro_phi_total     / L^2;
%