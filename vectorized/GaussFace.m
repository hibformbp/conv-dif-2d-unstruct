function [G]=GaussFace(order)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                    Artur Guilherme Vasconcelos
%                                       11 de Outubro de 2016
%                                       13 de Outubro de 2016
%                              ---------------------------------------
%                                      Leonardo S. R. Oliveira
%                                            17-mar-2021
%
% -- G = GaussFace (order)
%   Função para wrapper para determinar os Pontos de Gauss das faces.
%
%   O argumento de entrada determina a ordem do cálculo.
%   Os valores válidos são 2, 4, 6, 8.
%
%   O argumento de saída é uma matriz 3D de tamanho: 
%       n * 3 * face_num,
%   onde 'n' é o número de pontos de Gauss.
%
%   Caso seja pedido pontos de 2a ordem, esta função calcula-os
%   diretamente.  Caso contrário, esta chama a função 'gausspoints'
%   para tal.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells cell_verts cell_faces cell_vol faces face_area face_vert;
global face_bound cell_bound face_cells vert_cells;
%
type='1D';
%
%
%% CALCULAR COORDENADAS AUXILIARES %%
%
% Coordenadas do Vertice 1 %
x1 = verts(face_vert(:,1),1);
y1 = verts(face_vert(:,1),2);
%
% Coordenadas do Vertice 2 %
x2 = verts(face_vert(:,2),1);
y2 = verts(face_vert(:,2),2);
%
% Coordenadas do Centroide da Face %
x3 = faces(:,1);
y3 = faces(:,2);
%
%
%% CALCULAR COORDENADAS DOS PONTOS DE GAUSS %%
%
if order==2
    G = reshape([ x3'; y3'; ones(1,face_num) ], [1 3 face_num]);
else
    [G, n] = gausspoints(x1, y1, x2, y2, x3, y3, type, order);
end