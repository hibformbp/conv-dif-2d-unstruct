function [verts,cells,faces,cell_vert,cell_face,cell_vol,face_area,face_vert,cell_norm,cell_num,vert_num,face_num,Lref]=CartMesh1(uniforme)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos
%                                  16 de Outubro de 2016
%
% Função que cria uma malha Cartesiana
%
% verts     - Vetor que guarda as coordenadas dos vertices da malha         - verts(i)=(coordenada x, coordenada y);
% face_vert - Vetor que guarda os vertices das faces                        - face_vert(i)=(vertice 1, vertice 2);
% face_area - Vetor que guarda as areas dos vertices das faces              - face_area(i)=(area, area em x, area em y);
% faces     - Vetor que guarda as coordenadas do centroide da face          - faces(i)=(coordenada x, coordenada y)
% cell_vert - Vetor que guarada os vertices que compõem a celula            - cell_vert(i)=(vertice 1,vertice2,vertice4,vertice4)
% cell_face - Vetor que guarda as faces que compõem a celula                - cell_face(i)=(face 1,face 2,face 3,face 4)
% cell_vol  - Vetor que guarda o volume das células                         - cell_vol(i)=volume célula
% cells     - Vetor que guarda as coordendas do centroide da célula         - cells(i)=(coordenda x, coordenada y)
% cell_norm - Vetor que guarad as normais exteriores de cada face da célula - cell_norm(i)=(ne_x1,ne_y1,ne_x2,ne_y2,ne_x3,ne_y3,ne_x4,ne_y4,) 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%% Declaração das Variaveis Globais %%
%
global L cell_side vert_side;
%
%% Determinação de Proprieades Básicas %%
%
cell_num=cell_side^2;               % Numero Total de Células %
vert_num=vert_side^2;               % Numero Total de Vertices %
face_num=cell_side*vert_side*2;     % Numero Total de Faces %
%
Lref=sqrt(L*L/cell_num);            % Comprimento de Referencia da Malha %
%
%% Contrução da Malha Uniforme %%
%
% Coordenadas Gerais dos Vertices %
if uniforme
    aux = (0:vert_side-1).*Lref;
else
    error('\n\nERRO: Malha Não Implementada\n\n');
end
%
%% Determinação dos Vertices %%
%
[aux_i, aux_j] = meshgrid(aux,aux);
verts = [aux_i(:), aux_j(:)];
%
%% Determinação das Faces %%
%
ii = reshape(1:vert_side^2,vert_side,vert_side);
ii_1 = ii(1:end-1,:);
ii_2 = ii(2:end,:);
ii_3 = ii(:,1:end-1);
ii_4 = ii(:,2:end);
face_vert = [ ii_1(:) , ii_2(:) ; 
              ii_3(:) , ii_4(:) ];
ne = [ones(vert_side*(vert_side-1),1), zeros(vert_side*(vert_side-1),1)];
ne = [ne; [ne(:,2), ne(:,1)] ];
%
% Area das Faces e Coordenadas do Centroides das Faces %
%
x1=verts(face_vert(:,1),1);
x2=verts(face_vert(:,2),1);
y1=verts(face_vert(:,1),2);
y2=verts(face_vert(:,2),2);
%
% Area da Face %
face_area(:,1) = sqrt((x2-x1).^2+(y2-y1).^2);
face_area(:,2) = face_area(:,1).*ne(:,2);
face_area(:,3) = face_area(:,1).*ne(:,1);
%
% Coordenadas do Centroide da Face %
faces(:,1)=(x1+x2)./2;
faces(:,2)=(y1+y2)./2;
%
%% Determinação das Celulas %%
%
% Determinação dos Vertices da Celula %
ii_1 = ii(1:end-1,1:end-1);
ii_2 = ii(2:end,1:end-1);
ii_3 = ii(2:end,2:end);
ii_4 = ii(1:end-1,2:end);
cell_vert = [   ii_1(:), ...
                ii_2(:), ...
                ii_3(:), ...
                ii_4(:)  ];
%
% Determinação das Faces das Células %
[CV1, FV1] = meshgrid(cell_vert(:,1), face_vert(:,1));
[CV2, FV2] = meshgrid(cell_vert(:,2), face_vert(:,2));
[CV3, fv] = meshgrid(cell_vert(:,3), 1:face_num);
CV4 = meshgrid(cell_vert(:,4), face_vert(:,2));
cell_face = [ fv(FV1==CV1 & FV2==CV2), ...
              fv(FV1==CV2 & FV2==CV3), ...
              fv(FV2==CV3 & FV1==CV4), ...
              fv(FV2==CV4 & FV1==CV1)  ];
%
% Determinação dos Centroides das Células %
x1 = verts(cell_vert(:,1),1);
y1 = verts(cell_vert(:,1),2);
x2 = verts(cell_vert(:,2),1);
y2 = verts(cell_vert(:,2),2);
x3 = verts(cell_vert(:,3),1);
y3 = verts(cell_vert(:,3),2);
x4 = verts(cell_vert(:,4),1);
y4 = verts(cell_vert(:,4),2);
%
% Coordenadas do Centroide da Celula %
cells = [ x1+x2+x3+x4, y1+y2+y3+y4 ]./4;
%
% Volume da Celula %
aux1 = (x1.*y2 + x2.*y3 + x3.*y4 + x4.*y1);
aux2 = (y1.*x2 + y2.*x3 + y3.*x4 + y4.*x1);
cell_vol = abs(aux1-aux2)./2;
%
% Normais Exteriores das Faces da Célula %
cell_norm(:,1) = (y1-y2) ./ sqrt( (x2-x1).^2 + (y2-y1).^2 );
cell_norm(:,2) = (x2-x1) ./ sqrt( (x2-x1).^2 + (y2-y1).^2 );
%
cell_norm(:,3) = (y2-y3) ./ sqrt( (x3-x2).^2 + (y3-y2).^2 );
cell_norm(:,4) = (x3-x2) ./ sqrt( (x3-x2).^2 + (y3-y2).^2 );
%
cell_norm(:,5) = (y3-y4) ./ sqrt( (x3-x4).^2 + (y3-y4).^2 );
cell_norm(:,6) = (x4-x3) ./ sqrt( (x3-x4).^2 + (y3-y4).^2 );
%
cell_norm(:,7) = (y4-y1) ./ sqrt( (x4-x1).^2 + (y4-y1).^2 );
cell_norm(:,8) = (x1-x4) ./ sqrt( (x4-x1).^2 + (y4-y1).^2 );
%
end