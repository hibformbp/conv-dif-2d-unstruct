function [T,D]=Reconstruction4thOrder(stencil_cells,stencil_faces,stencil_size,ponderado)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                    Artur Guilherme Vasconcelos
%                                       06 de Outubro de 2016
%                              ---------------------------------------
%                                         Filipe J. M. Diogo
%                                                2019
%                              ---------------------------------------
%                                      Leonardo S. R. Oliveira
%                                            30-mar-2021
%
% -- Função que implementa a Solução Numérica a partir do Minimos Quadrados de 4ª Ordem
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


global face_num  w;
global verts cells faces face_vert ;
global face_bound face_cells face_area;
global phi extended_stencil neuman dimensional_correction robin u_convec_x u_convec_y gamma_diff;
%
% Criar cell arrays T, D
T  = cell(face_num, 1);
D  = cell(face_num, 1);
%
% Normal Exterior das Faces %
if extended_stencil
    nx = verts(face_vert(:,1),1) - verts(face_vert(:,2),1);
end
%
for i=1:face_num
    %
    %% CONSTRUÇÃO DA MATRIZ D PARA CADA FACE, CELULAS DO STENCIL %%
    %
    dx = cells(stencil_cells(i,1:stencil_size(i,2)),1) - faces(i,1);
    dy = cells(stencil_cells(i,1:stencil_size(i,2)),2) - faces(i,2);
    %
    if extended_stencil
        %
        % Matriz D %
        D{i} = zeros(stencil_size(i,1), 14);
        %
        D{i}(1:stencil_size(i,2),1) = 1;
        D{i}(1:stencil_size(i,2),2) = dx;
        D{i}(1:stencil_size(i,2),3) = dy;
        %
        D{i}(1:stencil_size(i,2),4) = dx.^2;
        D{i}(1:stencil_size(i,2),5) = dy.^2;
        D{i}(1:stencil_size(i,2),6) = dx.*dy;
        %
        D{i}(1:stencil_size(i,2),7)  = dx.^3;
        D{i}(1:stencil_size(i,2),8)  = dy.^3;
        D{i}(1:stencil_size(i,2),9)  = dx.^2 .* dy;
        D{i}(1:stencil_size(i,2),10) = dy.^2 .* dx;
        %
        if nx(i)==0
            D{i}(1:stencil_size(i,2),11) = dy.^4;
        else
            D{i}(1:stencil_size(i,2),11) = dx.^4;
        end
        %
        D{i}(1:stencil_size(i,2),12) = dx.^3 .* dy   ;
        D{i}(1:stencil_size(i,2),13) = dx.^2 .* dy.^2;
        D{i}(1:stencil_size(i,2),14) = dx    .* dy.^3;
        %
        % Matriz D tendo em conta a ponderação %
        if ponderado
            w = 1 ./ ( dx.^2 + dy.^2 ).^2 ;
            D1 = zeros(stencil_size(i,1), 14);
            D1(1:stencil_size(i,2),:) = repmat(w,1,14) .* D{i}(1:stencil_size(i,2),:);
        else
            D1 = D{i};
        end
        %
    else
        %
        % Matriz D %
        D{i} = zeros(stencil_size(i,1), 10);
        %
        D{i}(1:stencil_size(i,2),1) = 1;
        D{i}(1:stencil_size(i,2),2) = dx;
        D{i}(1:stencil_size(i,2),3) = dy;
        %
        D{i}(1:stencil_size(i,2),4) = dx.^2;
        D{i}(1:stencil_size(i,2),5) = dy.^2;
        D{i}(1:stencil_size(i,2),6) = dx.*dy;
        %
        D{i}(1:stencil_size(i,2),7)  = dx.^3;
        D{i}(1:stencil_size(i,2),8)  = dy.^3;
        D{i}(1:stencil_size(i,2),9)  = dx.^2 .* dy;
        D{i}(1:stencil_size(i,2),10) = dy.^2 .* dx;
        %
        % Matriz D tendo em conta a ponderação %
        if ponderado
            w = 1 ./ ( dx.^2 + dy.^2 ).^2 ;
            D1 = zeros(stencil_size(i,1), 10);
            D1(1:stencil_size(i,2),:) = repmat(w,1,10) .* D{i}(1:stencil_size(i,2),:);
        else
            D1 = D{i};
        end
        %
    end
    %
    %
    %% CONSTRUÇÃO DA MATRIZ D PARA CADA FACE, FACES DO STENCIL %%
    if stencil_size(i,3)~=0
        %
        dx = faces(stencil_faces(i,1:stencil_size(i,3)),1) - faces(i,1);
        dy = faces(stencil_faces(i,1:stencil_size(i,3)),2) - faces(i,2);
        %
        if ponderado
            wdx = dx;
            wdy = dy;
            wdx(i==stencil_faces(i,1:stencil_size(i,3))) = cells(face_cells(i,1),1) - faces(i,1);
            wdy(i==stencil_faces(i,1:stencil_size(i,3))) = cells(face_cells(i,1),2) - faces(i,2);
            %
            w = 1 ./ ( wdx.^2 + wdy.^2 ).^2 ;
        end
        %
        area_robin = face_area(stencil_faces(i,1:stencil_size(i,3)),1);
        normal_x = face_bound(stencil_faces(i,1:stencil_size(i,3)),2) .* area_robin;
        normal_y = face_bound(stencil_faces(i,1:stencil_size(i,3)),3) .* area_robin;
        if ~dimensional_correction
            normal_x = normal_x ./ area_robin;
            normal_y = normal_y ./ area_robin;
            area_robin(:) = 1;
        end
        %
        % NEUMANN
        %
        if neuman
            %
            D{i}((stencil_size(i,2)+1:end),2) = normal_x;
            %
            D{i}((stencil_size(i,2)+1:end),4) = normal_x .* 2.*dx ;
            D{i}((stencil_size(i,2)+1:end),6) = normal_x .*    dy ;
            %
            D{i}((stencil_size(i,2)+1:end),7)  = normal_x .* 3.*dx.^2  ;
            D{i}((stencil_size(i,2)+1:end),9)  = normal_x .* 2.*dx.*dy ;
            D{i}((stencil_size(i,2)+1:end),10) = normal_x .*    dy.^2 ;
            %
            if extended_stencil
                if nx(i)==0
                    D{i}((stencil_size(i,2)+1:end),11) = normal_y .* 4.*dy.^3 ;
                else
                    D{i}((stencil_size(i,2)+1:end),11) = normal_x .* 4.*dx.^3 ;
                end
                D{i}((stencil_size(i,2)+1:end),12) = normal_x .* 3.*dx.^2 .* dy    ...
                                                   + normal_y .*    dx.^3          ;
                D{i}((stencil_size(i,2)+1:end),13) = normal_x .* 2.*dx    .* dy.^2 ...
                                                   + normal_y .* 2.*dx.^2 .* dy    ;
                D{i}((stencil_size(i,2)+1:end),14) = normal_x .*             dy.^3 ...
                                                   + normal_y .* 3.*dx    .* dy.^2 ;
                %
                if ponderado
                    D1((stencil_size(i,2)+1:end),[2 4 6 7 9:14]) = repmat(w,1,10) .* D{i}((stencil_size(i,2)+1:end),[2 4 6 7 9:14]);
                else
                    D1((stencil_size(i,2)+1:end),[2 4 6 7 9:14]) = D{i}((stencil_size(i,2)+1:end),[2 4 6 7 9:14]);
                end
                %
            elseif ponderado
                D1((stencil_size(i,2)+1:end),[2 4 6 7 9 10]) = repmat(w,1,6) .* D{i}((stencil_size(i,2)+1:end),[2 4 6 7 9 10]);
            else
                D1((stencil_size(i,2)+1:end),[2 4 6 7 9 10]) = D{i}((stencil_size(i,2)+1:end),[2 4 6 7 9 10]);
            end
            %
        % ROBIN
        %
        elseif robin
            %
            k = gamma_diff/u_convec_x;
            %
            % formato: A*dx^i*dy^j + k*N, onde N=neumann
            %
            D{i}((stencil_size(i,2)+1:end),1) = area_robin;
            D{i}((stencil_size(i,2)+1:end),2) = area_robin.*dx + k*normal_x;
            D{i}((stencil_size(i,2)+1:end),3) = area_robin.*dy;
            %
            D{i}((stencil_size(i,2)+1:end),4) = area_robin.*dx.^2  + k*normal_x.*2.*dx;
            D{i}((stencil_size(i,2)+1:end),5) = area_robin.*dy.^2  ;
            D{i}((stencil_size(i,2)+1:end),6) = area_robin.*dx.*dy + k*normal_x.*dy ;
            %
            D{i}((stencil_size(i,2)+1:end),7)  = area_robin.*dx.^3     + k*normal_x.*3.*dx.^2;
            D{i}((stencil_size(i,2)+1:end),8)  = area_robin.*dy.^3     ;
            D{i}((stencil_size(i,2)+1:end),9)  = area_robin.*dx.^2.*dy + k*normal_x.*2.*dx.*dy ;
            D{i}((stencil_size(i,2)+1:end),10) = area_robin.*dy.^2.*dx + k*normal_x.*dy.^2 ;
            %
            if ponderado
                D1((stencil_size(i,2)+1:end),1:10) = repmat(w,1,10) .* D{i}((stencil_size(i,2)+1:end),1:10);
            else
                D1((stencil_size(i,2)+1:end),1:10) = D{i}((stencil_size(i,2)+1:end),1:10);
            end
            %
        % DIRICHLET
        %
        else
            %
            D{i}((stencil_size(i,2)+1:end),1) = 1;
            D{i}((stencil_size(i,2)+1:end),2) = dx;
            D{i}((stencil_size(i,2)+1:end),3) = dy;
            %
            D{i}((stencil_size(i,2)+1:end),4) = dx.^2;
            D{i}((stencil_size(i,2)+1:end),5) = dy.^2;
            D{i}((stencil_size(i,2)+1:end),6) = dx .* dy;
            %
            D{i}((stencil_size(i,2)+1:end),7)  = dx.^3;
            D{i}((stencil_size(i,2)+1:end),8)  = dy.^3;
            D{i}((stencil_size(i,2)+1:end),9)  = dx.^2 .* dy;
            D{i}((stencil_size(i,2)+1:end),10) = dy.^2 .* dx;
            %
            if extended_stencil
                if nx(i)==0
                    D{i}((stencil_size(i,2)+1:end),11) = dy.^4;
                else
                    D{i}((stencil_size(i,2)+1:end),11) = dx.^4;
                end
                D{i}((stencil_size(i,2)+1:end),12) = dy    .* dx.^3;
                D{i}((stencil_size(i,2)+1:end),13) = dy.^2 .* dx.^2;
                D{i}((stencil_size(i,2)+1:end),14) = dy.^3 .* dx;
                %
                if ponderado
                    D1((stencil_size(i,2)+1:end),:) = repmat(w,1,14) .* D{i}((stencil_size(i,2)+1:end),:);
                else
                    D1((stencil_size(i,2)+1:end),:) = D{i}((stencil_size(i,2)+1:end),:);
                end
                %
            elseif ponderado
                D1((stencil_size(i,2)+1:end),:) = repmat(w,1,10) .* D{i}((stencil_size(i,2)+1:end),:);
            else
                D1((stencil_size(i,2)+1:end),:) = D{i}((stencil_size(i,2)+1:end),:);
            end
        end
        %
    end
    %
    %
    %% DETERMINAÇÃO DA MATRIZ T %%
    % D*c=phi -> c=inv(D'D)*D'*phi -> T=inv(D'D)*D' %
    T{i} = inv( D{i}' * D1 ) * D1';    
end