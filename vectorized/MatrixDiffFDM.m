function [A,source,source_faces]=MatrixDiffFDM(dirichlet)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   04 de Julho de 2016                                             %
%                                                                                                   %
% Função que implementa a Solução Numérica a partir do Método de Diferenças Finitas de 2ª Ordem     %
% O Método Explicito ainda não foi implementado, sendo que o método implicito recorre à inversão da % 
% matriz                                                                                            %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%% TODO: error check for cell placement
%
global TempoGlobal;
global fid;
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells faces;
global cell_verts cell_faces cell_vol cell_norm cell_bound cell_vert_num cell_face_num;
global face_vert face_cells face_area face_bound;
global vert_cells vert_cell_num vert_face_num;
global phi lap_phi  phi_faces flux_phi_faces;
%
fprintf('\n\n\t\tConstrução da Matriz A\t\t\t Inicio ...');
tempo_A=cputime;
%
if !dirichlet
    error('\n\n\tERRO: Método Não Implementado\n\n');
end
%
%% Faces da Célula %%
face_w = cell_faces(:,1);
face_n = cell_faces(:,2);
face_e = cell_faces(:,3);
face_s = cell_faces(:,4);
%
%% Area das Faces %%
area_w = face_area(face_w,1);
area_e = face_area(face_e,1);
area_s = face_area(face_s,1);
area_n = face_area(face_n,1);
%
%% Distancias Entre Centroides %%
%
% Face de Fronteira Oeste x=0 %
bool_w = face_bound(face_w,1)==1;
cell_w = false(face_num,1);
cell_w(!bool_w) = true;
d_w(bool_w)  = cells(bool_w,1) - faces(face_w(bool_w),1);
d_w(!bool_w) = cells(!bool_w,1) - cells(face_cells(face_w(!bool_w),1),1);
d_w = d_w';
% TODO %
% if sum( face_cells(face_w(!bool_w),2)==cells() )
%     error('\n\n\tERRO: Esta celula não pode estar à esquerda da celula em estudo \n\n');
% end
%
% Face de Fronteira Este x=L %
bool_e = face_bound(face_e,1)==1;
cell_e = false(face_num, 1);
cell_e(!bool_e) = true;
d_e(bool_e)  = faces(face_e(bool_e),1) - cells(bool_e,1);
d_e(!bool_e) = cells(face_cells(face_e(!bool_e),2),1) - cells(!bool_e,1);
d_e = d_e';
% TODO %
% if sum( face_cells(face_w(!bool_w),2)==cells() )
%     error('\n\n\tERRO: Esta celula não pode estar à esquerda da celula em estudo \n\n');
% end
%
% Face de Fronteira Norte y=L %
bool_n = face_bound(face_n,1)==1;
cell_n = false(face_num, 1);
cell_n(!bool_n) = true;
d_n(bool_n)  = faces(face_n(bool_n),2) - cells(bool_n,2);
d_n(!bool_n) = cells(face_cells(face_n(!bool_n),2),2) - cells(!bool_n,2);
d_n = d_n';
% TODO %
% if sum( face_cells(face_w(!bool_w),2)==cells() )
%     error('\n\n\tERRO: Esta celula não pode estar à esquerda da celula em estudo \n\n');
% end
%
% Face de Fronteira Sul y=0 %
bool_s = face_bound(face_s,1)==1;
cell_s = false(face_num, 1);
cell_s(!bool_s) = true;
d_s(bool_s)  = cells(bool_s,2) - faces(face_s(bool_s),2);
d_s(!bool_s) = cells(!bool_s,2) - cells(face_cells(face_s(!bool_s),1),2);
d_s = d_s';
% TODO %
% if sum( face_cells(face_w(!bool_w),2)==cells() )
%     error('\n\n\tERRO: Esta celula não pode estar à esquerda da celula em estudo \n\n');
% end
%
%% Determinação dos Termos Fonte tendo já em conta as celulas de fronteira %%
% Determinação da Matriz A %
aux_d = - ( area_w./d_w + area_e./d_e + area_n./d_n + area_s./d_s );
i_d = (1:cell_num)';
j_d = i_d;
%
source_w = zeros(cell_num,1);
source_w(bool_w) = area_w(bool_w) ./ d_w(bool_w) ...
                    .* phi_faces(face_w(bool_w));
aux_w = area_w(!bool_w) ./ d_w(!bool_w);
i_w = i_d(!bool_w);
j_w = i_d(!bool_w) - cell_side;
%
source_e = zeros(cell_num,1);
source_e(bool_e) = area_e(bool_e) ./ d_e(bool_e) ...
                    .* phi_faces(face_e(bool_e));
aux_e = area_e(!bool_e) ./ d_e(!bool_e);
i_e = i_d(!bool_e);
j_e = i_d(!bool_e) + cell_side;
%
source_n = zeros(cell_num,1);
source_n(bool_n) = area_n(bool_n) ./ d_n(bool_n) ...
                    .* phi_faces(face_n(bool_n));
aux_n = area_n(!bool_n) ./ d_n(!bool_n);
i_n = i_d(!bool_n);
j_n = i_d(!bool_n) + 1;
%
source_s = zeros(cell_num,1);
source_s(bool_s) = area_s(bool_s) ./ d_s(bool_s) ...
                    .* phi_faces(face_s(bool_s));
aux_s = area_s(!bool_s) ./ d_s(!bool_s);
i_s = i_d(!bool_s);
j_s = i_d(!bool_s) - 1;
%
source_faces = source_w + source_e + source_n + source_s;
source = lap_phi.*cell_vol - source_faces;
%
A = sparse( [i_d; i_w; i_e; i_n; i_s], ...
            [j_d; j_w; j_e; j_n; j_s], ...
            [aux_d; aux_w; aux_e; aux_n; aux_s], ...
            cell_num, cell_num );
%
fprintf('... Fim\n');