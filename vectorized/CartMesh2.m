function [face_cells,vert_cells,face_bound,cell_bound,cell_vert_num,cell_face_num,vert_cell_num,vert_face_num]=CartMesh2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                 16 de Outubro de 2016                                             %
%                                                                                                   %
% Função que determina as propriedades da malha                                                     %
%                                                                                                   %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells faces;
global cell_verts cell_faces cell_vol cell_norm;
global face_vert face_area;
global metodo;
%
%% Determinação das Faces de Fronteira do Dominio %%
%
% Alterar o tipo de fronteiras
face_bound = false([face_num, 3]);
face_bound(faces(:,1)==0 | faces(:,1)==L, 2) = true;
face_bound(faces(:,2)==0 | faces(:,2)==L, 3) = true;
face_bound(:,1) = face_bound(:,2) | face_bound(:,3);
%
%
%
%% Determinação das Células de Fronteira do Dominio %%
%
n=size(cell_faces,2);
m=size(cell_verts,2);
%
% cell_bound = face_bound(cell_faces(:,1),:);
% for i=2:n
%     cell_bound = cell_bound | face_bound(cell_faces(:,i),:);
% end
cell_bound = or(    ...
                or( ...
                    face_bound(cell_faces(:,1),:), ...
                    face_bound(cell_faces(:,2),:)  ...
                  ),...
                or( ...
                    face_bound(cell_faces(:,3),:), ...
                    face_bound(cell_faces(:,4),:)  ...
                  ) ...
                );
%
cell_face_num = sum(cell_faces~=0, 2);
cell_vert_num = sum(cell_verts~=0, 2);
%
%
%
%% Determinação das Células que contém a Face %%
%
face_cells = zeros(face_num, 2);
%
aux = (1:face_num);
%
[aux_faces, aux_cells_1] = intersect(cell_faces(:,1:2), aux(~face_bound(:,1)));
[~,         aux_cells_2] = intersect(cell_faces(:,3:4), aux(~face_bound(:,1)));
[aux_cells_1, ~] = ind2sub(size(cell_faces),aux_cells_1);
[aux_cells_2, ~] = ind2sub(size(cell_faces),aux_cells_2);
face_cells(aux_faces,:) = [aux_cells_1, aux_cells_2];
face_cells(face_cells(:,2)~=0,:) = sort(face_cells(face_cells(:,2)~=0,:), 2, 'ascend');
%
[aux_faces, aux_cells_1] = intersect(cell_faces, aux(face_bound(:,1)));
[aux_cells_1, ~] = ind2sub(size(cell_faces),aux_cells_1);
face_cells(aux_faces,1) = aux_cells_1;
%
%
%
%% Determinação das Células que contém o Vertice %%
%
vert_cells = zeros(vert_num, 4);
%
for i=1:4
    vert_cells(cell_verts(:,i),i) = 1:cell_num;
end
%
vert_cells = sort(vert_cells,2,'descend');
%
vert_cell_num = sum(vert_cells~=0, 2);
%
%
%
%% Determinação das Faces que contém o Vertice %%
%
vert_face_num = zeros(vert_num, 1);
%
for i=1:vert_num
    vert_face_num(i) = sum(sum(face_vert==i));
end
%
%
