function f = SolutionDiffusion(solution,x,y,type)
global u_convec_x u_convec_y gamma_diff;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   28 de Junho de 2016                                             %
%                                  12 de Outubro de 2016                                            %
%                                                                                                   %
% Função que determina as soluções analiticas                                                       %
%                                                                                                   %
% solution - Função Analítica que se pretende calcular                                              %
% x        - Coordenada X                                                                           %
% y        - Coordenada Y                                                                           %
% type     - Escolhe entre a solução analítica 'anal', laplaciano 'lap' e as derivadas em x e y     %
%            'xflux' ou 'yflux'                                                                     %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
switch solution
case 'sin'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = sin(3*pi*x) .* sin(3*pi*y);
    %
    % Laplaciano %
    case 'lap'
        f = -18*pi^2*sin(3*pi*x).*sin(3*pi*y);
    %
    % Derivada X %
    case 'xflux'
        f = 3*pi*cos(3*pi*x).*sin(3*pi*y);
    %
    % Derivada Y %
    case 'yflux'
        f = 3*pi*sin(3*pi*x).*cos(3*pi*y);
    %
    end
%
case 'sin2'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = 50000*cos(3*pi*x).*sin(2*pi*y);
    %
    % Laplaciano %
    case 'lap'
        f = -9*50000*pi^2*cos(3*pi*x).*sin(2*pi*y) ...
            -50000*4*pi^2*cos(3*pi*x).*sin(2*pi*y);
    %
    % Derivada X %
    case 'xflux'
        f = -3*50000*pi*sin(3*pi*x).*sin(2*pi*y);
    %
    % Derivada Y %
    case 'yflux'
        f = 2*50000*pi*cos(3*pi*x).*cos(2*pi*y);
    %
    end
%    
case 'exp'
    %
    % Constantes %
    s2=0.0175;
    s=sqrt(s2);
    u=0.5;
    %
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = exp(-((x-u).^2+(y-u).^2)/s2);
    %
    % Laplaciano %
    case 'lap'
        f = 4/s2* ( (x-u).^2/s2 + (y-u).^2/s2 - 1 ) ...
            .* exp(-((x-u).^2+(y-u).^2)/s2);
        %-4/s2*(1-((x-u)^2+(y-u)^2)/s2)*exp(((x-u)^2+(y-u)^2)/s2);
    %
    % Derivada X %
    case 'xflux'
        f = -2*(x-u)/s2.*exp(-((x-u).^2+(y-u).^2)/s2);
    %
    % Derivada Y %
    case 'yflux'
        f = -2*(y-u)/s2.*exp(-((x-u).^2+(y-u).^2)/s2);
    %
    end
%   
%% Em teste    
case 'paper'
    %
    % Constantes %
    C = 65; %11236
    u = u_convec_x;
    v = u_convec_y;
    k = -gamma_diff; %difusivo
    %
    % funções auxiliares %
    alpha =@(x) (x - (exp(u.*x)-1)./(exp(u)-1) ) ./ u;
    beta = @(y) (y - (exp(v.*y)-1)./(exp(v)-1) ) ./ v;
    %
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = C*alpha(x).*beta(y);
    %
    % Laplaciano %
    case 'lap'
        %f = -C*(alpha(x)+beta(y));
        f =   C*beta(y) .* ( 1 + (k-1)*u.*exp(u.*x)./(exp(u)-1)) ...
            + C*alpha(x).* ( 1 + (k-1)*v.*exp(v.*y)./(exp(v)-1));
    %
    % Derivada X %
    case 'xflux'
        f = zeros(size(x));
    %
    % Derivada Y %
    case 'yflux'
        f = zeros(size(y));
    %
    end
%% FIm teste
%
case '2nd'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = 0.5*(x+y);
    %
    % Laplaciano %
    case 'lap'
        f = zeros(size(x));
    %
    % Derivada X %
    case 'xflux'
        f = 0.5;
    %
    % Derivada Y %
    case 'yflux'
        f = 0.5;
    %
    end
%
case '4th'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = (y-1).^3 + (x-1).^3;
    %
    % Laplaciano %
    case 'lap'
        f = 6*( y + x - 2 );
    %
    % Derivada X %
    case 'xflux'
        f = 3*(x-1).^2;
    %
    % Derivada Y %
    case 'yflux'
        f = 3*(y-1).^2;
    %
    end
%
case '6th'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = x.^5;
    %
    % Laplaciano %
    case 'lap'
        f = 20*x.^3;
    %
    % Derivada X %
    case 'xflux'
        f = 5*x.^4;
    %
    % Derivada Y %
    case 'yflux'
        f = zeros(size(x));
    %
    end
%
case '8th'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = y.^3;
    %
    % Laplaciano %
    case 'lap'
        f = 6*y;
    %
    % Derivada X %
    case 'xflux'
        f = zeros(size(y));
    %
    % Derivada Y %
    case 'yflux'
        f = 3*y.^2;
    %
    end
%
case '9th'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = (x.*y).^7 .* ( x + y );
    %
    % Laplaciano %
    case 'lap'
        f = (x.*y).^5 .* (  42*y.^3 + 56*x.*(y.^2) ...
                          + 42*x.^3 + 56*y.*(x.^2)  ) ;
    %
    % Derivada X %
    case 'xflux'
        f = (x.*y).^6 .* (  7*y.^2 + 8*x.*y );
    %
    % Derivada Y %
    case 'yflux'
        f = (x.*y).^6 .* (  7*x.^2 + 8*x.*y );
    %
    end
%
case '10th'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = (x.*y).^7;
    %
    % Laplaciano %
    case 'lap'
        f = 42*(x.*y).^5 .* ( y.^2 + x.^2 );
    %
    % Derivada X %
    case 'xflux'
        f = 7*y .* (x.*y).^6;
    %
    % Derivada Y %
    case 'yflux'
        f = 7*x .* (x.*y).^6;
    %
    end
%
case 'paper2'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = 4*y.*(1-y)./(x+1);
    %
    % Laplaciano %
    case 'lap'
        f = -8* ( (y-1).*y./(x+1).^2 + 1 ) ./ (x+1);
    %
    % Derivada X %
    case 'xflux'
        f = 4*(y-1).*y./(x+1).^2;
    %
    % Derivada Y %
    case 'yflux'
        f = (4-8*y)./(x+1);
    %
    end
%
case '11th'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = exp(x);
    %
    % Laplaciano %
    case 'lap'
        f = exp(x);
    %
    % Derivada X %
    case 'xflux'
        f = exp(x);
    %
    % Derivada Y %
    case 'yflux'
        f = zeros(size(x));
    %
    end       
%
case 'poly3'
    switch type
    %
    % Solução Analítica %
    case 'anal'
        f = 1 + x + y + x.^2 + y.^2 + x.*y + x.^3 + y.^3;
            %+1*x^2*y+1*y^2*x;
    %
    % Laplaciano %
    case 'lap'
        f = 4 + 6*(x+y);
            %+2*x+2*y;
    %
    % Derivada X %
    case 'xflux'
        f = 1 + 2*x + y + 3*x.^2;
            %+2*x*y+y^2;
    %
    % Derivada Y %
    case 'yflux'
        f = 1 + 2*y + x + 3*y.^2;
            %+x^2+2*x*y;
    %
    end
%
end