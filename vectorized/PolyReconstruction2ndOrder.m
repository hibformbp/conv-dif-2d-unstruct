function [f, g] = PolyReconstruction2ndOrder(C,aux_xg,aux_yg,aux_xc,aux_yc,type,face,ind)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                    Artur Guilherme Vasconcelos
%                                       14 de Outubro de 2016
%                              ---------------------------------------
%                                         Filipe J. M. Diogo
%                                                2019
%                              ---------------------------------------
%                                      Leonardo S. R. Oliveira
%                                            16-mar-2021
%
% -- Calcula a contirbuição de cada celula para a reconstrução do polinomio na face 
%
%    Esta função tem como objectivo facilitar a construção da matriz A.
%
%    Atenção que esta função não determina o polinomio reconstruido na face, serve apenas para
%    quando se esta a determinar as entradas na matriz A.
%
%    Caso queira saber o polinomio da face, usar outra função.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


global verts face_vert extended_stencil;
%
%% Calcular outputs
switch type
case 'flux'
    %
    if extended_stencil
        % Pre-calculo da Normal Exterior das Faces (vetores) %
        aux_nx = verts(face_vert(face,1),1) - verts(face_vert(face,2),1);
        aux_ny = verts(face_vert(face,1),2) - verts(face_vert(face,2),2);
        %
        % Vetores auxiliares
        nx = aux_nx(ind); % vetor: normal exterior das faces
        ny = aux_ny(ind);
        xc = aux_xc(ind); % vetor: coordenadas dos centroides
        yc = aux_yc(ind);
        xg = aux_xg(ind); % "matriz": coordenadas dos pontos de gauss
        yg = aux_yg(ind); %    na vdd, eh um vetor pois ng=1 p/ 2a ordem
        %
        nx_0 = (nx==0);
        ny_0 = (ny==0);
        %
        f = zeros(size(C,1),1);
        g = zeros(size(C,1),1);
        %
        f(nx_0) = C(nx_0,2) + C(nx_0,5).*( yg(nx_0) - yc(nx_0) ) ;
        f(ny_0) = C(ny_0,2) + C(ny_0,5).*( yg(ny_0) - yc(ny_0) ) ...
                        + 2*C(ny_0,4).* ( xg(ny_0) - xc(ny_0) )    ;
        %
        g(ny_0) = C(ny_0,3) + C(ny_0,5).*( xg(ny_0) - xc(ny_0) ) ;
        g(nx_0) = C(nx_0,3) + C(ny_0,5).*( xg(ny_0) - xc(ny_0) ) ...
                        + 2*C(nx_0,4).*( yg(nx_0) - yc(nx_0) )     ;
    else
        f = C(:,2);
        g = C(:,3);
    end
    %
    %
case 'poly'
    % Pre-calculo da Normal Exterior das Faces (vetores) %
    aux_nx = verts(face_vert(face,1),1) - verts(face_vert(face,2),1);
    aux_ny = verts(face_vert(face,1),2) - verts(face_vert(face,2),2);
    %
    % Vetores auxiliares
    nx = aux_nx(ind); % vetor: normal exterior das faces
    ny = aux_ny(ind);
    xc = aux_xc(ind); % vetor: coordenadas dos centroides
    yc = aux_yc(ind);
    xg = aux_xg(ind); % "matriz": coordenadas dos pontos de gauss
    yg = aux_yg(ind); %    na vdd, eh um vetor pois ng=1 p/ 2a ordem
    %
    nx_0 = (nx==0);
    ny_0 = (ny==0);
    %
    g = [];
    f = C(:,1) + C(:,2).*( xg - xc ) + C(:,3).*( yg - yc);
    %
    if extended_stencil
        f(nx_0) = f(nx_0) + C(nx_0,4).*( yg(nx_0) - yc(nx_0) ).^2 ...
                    + C(nx_0,5).*( yg(nx_0) - yc(nx_0) ).*( xg(nx_0) - xc(nx_0) );
        f(ny_0) = f(ny_0) + C(ny_0,4).*( xg(ny_0) - xc(ny_0) ).^2 ...
                    + C(ny_0,5).*( yg(ny_0) - yc(ny_0) ).*( xg(ny_0) - xc(ny_0) );
    end
    % 
case 'xflux'
    g = [];
    %
    if extended_stencil
        % Pre-calculo da Normal Exterior das Faces (vetores) %
        aux_nx = verts(face_vert(face,1),1) - verts(face_vert(face,2),1);
        aux_ny = verts(face_vert(face,1),2) - verts(face_vert(face,2),2);
        %
        % Vetores auxiliares
        nx = aux_nx(ind); % vetor: normal exterior das faces
        ny = aux_ny(ind);
        xc = aux_xc(ind); % vetor: coordenadas dos centroides
        yc = aux_yc(ind);
        xg = aux_xg(ind); % "matriz": coordenadas dos pontos de gauss
        yg = aux_yg(ind); %    na vdd, eh um vetor pois ng=1 p/ 2a ordem
        %
        nx_0 = (nx==0);
        ny_0 = (ny==0);
        %
        f = zeros(size(C,1),1);
        %
        f(nx_0) = C(nx_0,2) + C(nx_0,5).*( yg(nx_0) - yc(nx_0) ) ;
        f(ny_0) = C(ny_0,2) + C(ny_0,5).*( yg(ny_0) - yc(ny_0) ) ...
                        + 2*C(ny_0,4).* ( xg(ny_0) - xc(ny_0) )    ;
    else
        f = C(:,2);
    end
    %
    %
case 'yflux'
    g = [];
    %
    if extended_stencil
        % Pre-calculo da Normal Exterior das Faces (vetores) %
        aux_nx = verts(face_vert(face,1),1) - verts(face_vert(face,2),1);
        aux_ny = verts(face_vert(face,1),2) - verts(face_vert(face,2),2);
        %
        % Vetores auxiliares
        nx = aux_nx(ind); % vetor: normal exterior das faces
        ny = aux_ny(ind);
        xc = aux_xc(ind); % vetor: coordenadas dos centroides
        yc = aux_yc(ind);
        xg = aux_xg(ind); % "matriz": coordenadas dos pontos de gauss
        yg = aux_yg(ind); %    na vdd, eh um vetor pois ng=1 p/ 2a ordem
        %
        nx_0 = (nx==0);
        ny_0 = (ny==0);
        %
        f = zeros(size(C,1),1);
        %
        f(ny_0) = C(ny_0,3) + C(ny_0,5).*( xg(ny_0) - xc(ny_0) ) ;
        f(nx_0) = C(nx_0,3) + C(ny_0,5).*( xg(ny_0) - xc(ny_0) ) ...
                        + 2*C(nx_0,4).*( yg(nx_0) - yc(nx_0) )     ;
    else
        f = C(:,3);
    end
    %
otherwise
    error('\n\n\tERRO: Fluxo Desconhecido\n\n');
end