function [T,D]=Reconstruction2ndOrder(stencil_cells,stencil_faces,stencil_size,ponderado)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                    Artur Guilherme Vasconcelos
%                                       26 de Setembro de 2016
%                              ---------------------------------------
%                                         Filipe J. M. Diogo
%                                                2019
%                              ---------------------------------------
%                                      Leonardo S. R. Oliveira
%                                            16-mar-2021
%                                                 
% -- Função que implementa a Solução Numérica a partir do Minimos Quadrados de 2ª Ordem
%                                                 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% TODO: 
%   -- realizar calculo somente 1 vez p stencils internos, em malha cartesiana uniforme
%   -- verificar se BC (neumann & robin) so implementada p/ faces verticais (?!) ou se assumiu-se u=(ux,0)
%
global L Lref cell_side vert_side cell_num face_num vert_num w;
global verts cells cell_verts cell_faces cell_vol faces face_area face_vert cell_norm;
global face_bound cell_bound face_cells vert_cells u_convec_x u_convec_y gamma_diff;
global phi lap_phi  phi_faces flux_phi_faces extended_stencil neuman dimensional_correction robin;
%
% Criar cell arrays T, D
T  = cell(face_num, 1);
D  = cell(face_num, 1);
%
% Normal Exterior das Faces %
if extended_stencil
    nx = verts(face_vert(:,1),1) - verts(face_vert(:,2),1);
end
%
for i=1:face_num
    %
    %% CONSTRUÇÃO DA MATRIZ D PARA CADA FACE, CELULAS DO STENCIL %%
    %
    dx = cells(stencil_cells(i,1:stencil_size(i,2)),1) - faces(i,1);
    dy = cells(stencil_cells(i,1:stencil_size(i,2)),2) - faces(i,2);
    %
    if extended_stencil
        %
        % Matriz D %
        D{i} = zeros(stencil_size(i,1), 5);
        %
        D{i}(1:stencil_size(i,2),1) = 1;
        D{i}(1:stencil_size(i,2),2) = dx;
        D{i}(1:stencil_size(i,2),3) = dy;
        %
        if nx(i)==0
            D{i}(1:stencil_size(i,2),4) = dy.^2;
        else
            D{i}(1:stencil_size(i,2),4) = dx.^2;
        end
        %
        D{i}(1:stencil_size(i,2),5) = dx .* dy;
        %
        % Matriz D tendo em conta a ponderação %
        if ponderado
            w = 1 ./ ( dx.^2 + dy.^2 );
            D1 = zeros(stencil_size(i,1), 5);
            D1(1:stencil_size(i,2),:) = repmat(w,1,5) .* D{i}(1:stencil_size(i,2),:);
        else
            D1 = D{i};
        end
        %
    else
        %
        % Matriz D %
        D{i} = zeros(stencil_size(i,1), 3);
        %
        D{i}(1:stencil_size(i,2),1) = 1;
        D{i}(1:stencil_size(i,2),2) = dx;
        D{i}(1:stencil_size(i,2),3) = dy;
        %
        % Matriz D tendo em conta a ponderação %
        if ponderado
            w = 1 ./ ( dx.^2 + dy.^2 );
            D1 = zeros(stencil_size(i,1), 3);
            D1(1:stencil_size(i,2),:) = repmat(w,1,3) .* D{i}(1:stencil_size(i,2),:);
        else
            D1 = D{i};
        end
        %
    end
    %
    %
    %% CONSTRUÇÃO DA MATRIZ D PARA CADA FACE, FACES DO STENCIL %%
    if stencil_size(i,3)~=0
        %
        dx = faces(stencil_faces(i,1:stencil_size(i,3)),1) - faces(i,1);
        dy = faces(stencil_faces(i,1:stencil_size(i,3)),2) - faces(i,2);
        %
        if ponderado
            wdx = dx;
            wdy = dy;
            wdx(i==stencil_faces(i,1:stencil_size(i,3))) = cells(face_cells(i,1),1) - faces(i,1);
            wdy(i==stencil_faces(i,1:stencil_size(i,3))) = cells(face_cells(i,1),2) - faces(i,2);
            %
            w = 1 ./ ( wdx.^2 + wdy.^2 );
        end
        %
        area_robin = face_area(stencil_faces(i,1:stencil_size(i,3)),1);
        normal_x = face_bound(stencil_faces(i,1:stencil_size(i,3)),2) .* area_robin;
        normal_y = face_bound(stencil_faces(i,1:stencil_size(i,3)),3) .* area_robin;
        if ~dimensional_correction
            normal_x = normal_x ./ area_robin;
            normal_y = normal_y ./ area_robin;
            area_robin(:) = 1;
        end
        %
        % NEUMANN
        %
        if neuman
            %
            D{i}((stencil_size(i,2)+1:end),2) = normal_x;
            %
            if extended_stencil
                if nx(i)==0
                    D{i}((stencil_size(i,2)+1:end),4) = 2.*dy.*normal_y;
                else
                    D{i}((stencil_size(i,2)+1:end),4) = 2.*dx.*normal_x;
                end
                D{i}((stencil_size(i,2)+1:end),5) = dy.*normal_x + dx.*normal_y;
                %
                if ponderado
                    D1((stencil_size(i,2)+1:end),[2 4 5]) = [w w w] .* D{i}((stencil_size(i,2)+1:end),[2 4 5]);
                else
                    D1((stencil_size(i,2)+1:end),[2 4 5]) = D{i}((stencil_size(i,2)+1:end),[2 4 5]);
                end
                %
            elseif ponderado
                D1((stencil_size(i,2)+1:end),2) = w .* D{i}((stencil_size(i,2)+1:end),2);
            else
                D1((stencil_size(i,2)+1:end),2) = D{i}((stencil_size(i,2)+1:end),2);
            end
            %
        % ROBIN
        %
        elseif robin
            %
            D{i}((stencil_size(i,2)+1:end),1) = area_robin;
            D{i}((stencil_size(i,2)+1:end),2) = (gamma_diff/u_convec_x)*normal_x + area_robin.*dx;
            D{i}((stencil_size(i,2)+1:end),3) = area_robin.*dy;
            %
            % if extended_stencil
            %     if nx(i)==0
            %         D{i}((stencil_size(i,2)+1:end),4) = 2.*dy.*normal_y;
            %     else
            %         D{i}((stencil_size(i,2)+1:end),4) = 2.*dx.*normal_x;
            %     end
            %     D{i}((stencil_size(i,2)+1:end),5) = dy.*normal_x + dx.*normal_y;
            %     %
            %     if ponderado
            %         D1((stencil_size(i,2)+1:end),[2 4 5]) = [w w w] .* D{i}((stencil_size(i,2)+1:end),[2 4 5]);
            %     else
            %         D1((stencil_size(i,2)+1:end),[2 4 5]) = D{i}((stencil_size(i,2)+1:end),[2 4 5]);
            %     end
            %     %
            % elseif ponderado
            if ponderado
                D1((stencil_size(i,2)+1:end),1:3) = [w w w] .* D{i}((stencil_size(i,2)+1:end),1:3);
            else
                D1((stencil_size(i,2)+1:end),1:3) = D{i}((stencil_size(i,2)+1:end),1:3);
            end
            %
        % DIRICHLET
        %
        else
            %
            D{i}((stencil_size(i,2)+1:end),1) = 1;
            D{i}((stencil_size(i,2)+1:end),2) = dx;
            D{i}((stencil_size(i,2)+1:end),3) = dy;
            %
            if extended_stencil
                if nx(i)==0
                    D{i}((stencil_size(i,2)+1:end),4) = dy.^2;
                else
                    D{i}((stencil_size(i,2)+1:end),4) = dx.^2;
                end
                D{i}((stencil_size(i,2)+1:end),5) = dy.*dx;
                %
                if ponderado
                    D1((stencil_size(i,2)+1:end),:) = [w w w w w] .* D{i}((stencil_size(i,2)+1:end),:);
                else
                    D1((stencil_size(i,2)+1:end),:) = D{i}((stencil_size(i,2)+1:end),:);
                end
                %
            elseif ponderado
                D1((stencil_size(i,2)+1:end),1:3) = [w w w] .* D{i}((stencil_size(i,2)+1:end),1:3);
            else
                D1((stencil_size(i,2)+1:end),1:3) = D{i}((stencil_size(i,2)+1:end),1:3);
            end
        end
        %
    end
    %
    %
    %% DETERMINAÇÃO DA MATRIZ T %%
    % D*c=phi -> c=inv(D'D)*D'*phi -> T=inv(D'D)*D' %
    T{i} = inv( D{i}' * D1 ) * D1';
end