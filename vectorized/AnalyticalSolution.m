function [phi,lap_phi,phi_faces,flux_phi_faces]=AnalyticalSolution(solution,metodo,equation)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   28 de Junho de 2016                                             %
%                                                                                                   %
% Função que determina as soluções analiticas                                                       %
%                                                                                                   %
% phi               - Valor da Propriedade no centroide da célula;                                  %
% lap_phi           - Laplaciano da Propriedade na célula, utiliza pontos de Gauss;                 %
% phi_faces         - Valor da Propriedade nas fronteiras;                                          %
% flux_phi_faces    - Valor do Fluxo nas fronteiras, já está a multiplicar pela area da face;       %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TODO: WLS_6, WLS_8
%
global face_num vert_num cell_num;
global verts cells faces;
global cell_verts cell_vol cell_vert_num;
global face_vert face_area face_bound;
global u_convec_x u_convec_y gamma_diff;
%
%% Celulas do Dominio %%
n_vert = 4;

% Função Analitica %
phi = SolutionDiffusion(solution,cells(:,1),cells(:,2),'anal');
%
% Calcula o Laplaciano de Phi com base nos Pontos de Gauss %
switch metodo
    case {'FDM_2' 'WLS_2'}
        %
        % 2ª Ordem %
        if strcmp(solution,'paper')==1
            lap_phi = SolutionDiffusion(solution, ...
                        cells(:,1),cells(:,2),'lap');   
        else
            % lap_phi = SolutionDiffusion(solution, ...
            %             cells(:,1),cells(:,2),'lap');   
            lap_phi = gamma_diff.*SolutionDiffusion(solution,cells(:,1),cells(:,2),'lap') ...
                    + u_convec_x.*SolutionDiffusion(solution,cells(:,1),cells(:,2),'xflux') ...
                    + u_convec_y.*SolutionDiffusion(solution,cells(:,1),cells(:,2),'yflux');
        end
        %
    case 'WLS_4' %% TODO %%
        %
        % 4ª Ordem %
        % Calula o Laplaciano de Phi com base nos Pontos de Gauss %
        aux_lap = zeros(cell_num, n_vert);
        aux = 1:n_vert;
        aux_sh = circshift(aux, -1);
        %
        % Cada Celula é dividida em Triangulos %
        x3 = cells(:,1);
        y3 = cells(:,2);
        for i = aux
            x1 = verts( cell_verts(:,i), 1);
            y1 = verts( cell_verts(:,i), 2);
            x2 = verts( cell_verts(:,aux_sh(i)), 1);
            y2 = verts( cell_verts(:,aux_sh(i)), 2);
            %
            vol_tri = abs( x1.*(y2-y3) + x3.*(y1-y2) + x2.*(y3-y1) ) ./ 2;
            %
            % Pontos de Gauss %
            [G, ngauss] = gausspoints( x1, y1, x2, y2, x3, y3, '2D', 4);
            %
            if strcmp(solution,'paper')==1
                aux_sol = SolutionDiffusion( solution, G(:,1,:), G(:,2,:), 'lap');
            else
                aux_sol = gamma_diff.*SolutionDiffusion( solution, G(:,1,:), G(:,2,:), 'lap')...
                        + u_convec_x.*SolutionDiffusion( solution, G(:,1,:), G(:,2,:), 'xflux') ...
                        + u_convec_y.*SolutionDiffusion( solution, G(:,1,:), G(:,2,:), 'yflux');
            end
            %
            aux_l = sum(aux_sol.*G(:,3,:), 1);
            aux_lap(:,i) = aux_l(:) .* vol_tri;
        end
        lap_phi = sum(aux_lap,2) ./ cell_vol;
        %
    % case 'WLS_6' %% TODO %%
        %
        % 6ª Ordem %
        %
        % Calula o Laplaciano de Phi com base nos Pontos de Gauss %
        % lap_phi(i)=0;
        % for j=1:cell_vert_num(i)
        %     aux1=0;
        %     if j==cell_vert_num(i)
        %         x1=verts(cell_verts(i,j),1);
        %         y1=verts(cell_verts(i,j),2);
        %         x2=verts(cell_verts(i,1),1);
        %         y2=verts(cell_verts(i,1),2);
        %         x3=cells(i,1);
        %         y3=cells(i,2);
        %     else
        %         x1=verts(cell_verts(i,j),1);
        %         y1=verts(cell_verts(i,j),2);
        %         x2=verts(cell_verts(i,j+1),1);
        %         y2=verts(cell_verts(i,j+1),2);
        %         x3=cells(i,1);
        %         y3=cells(i,2);
        %     end
        %     %
        %     vol_tri=abs(x1*y2+x3*y1+x2*y3-(x3*y2+x1*y3+x2*y1))/2;
        %     %
        %     % Pontos de Gauss %
        %     [x_gauss,ngauss]=gausspoints(x1,y1,x2,y2,x3,y3,'2D',6);
        %     %
        %     for k=1:ngauss
        %         if strcmp(solution,'paper')==1
        %         aux2=SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'lap');
        %         else
        %         aux2=gamma_diff*SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'lap')+u_convec_x*SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'xflux')+u_convec_y*SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'yflux'); 
        %         end
        %         aux1=aux1+aux2*x_gauss(k,3)*vol_tri/cell_vol(i);
        %     end
        %     lap_phi(i)=lap_phi(i)+aux1;
        % end
    % case 'WLS_8' %% TODO %%
        %
        % 8ª Ordem %
        %
        % Calula o Laplaciano de Phi com base nos Pontos de Gauss %
        % lap_phi(i)=0;
        % for j=1:cell_vert_num(i)
        %     aux1=0;
        %     if j==cell_vert_num(i)
        %         x1=verts(cell_verts(i,j),1);
        %         y1=verts(cell_verts(i,j),2);
        %         x2=verts(cell_verts(i,1),1);
        %         y2=verts(cell_verts(i,1),2);
        %         x3=cells(i,1);
        %         y3=cells(i,2);
        %     else
        %         x1=verts(cell_verts(i,j),1);
        %         y1=verts(cell_verts(i,j),2);
        %         x2=verts(cell_verts(i,j+1),1);
        %         y2=verts(cell_verts(i,j+1),2);
        %         x3=cells(i,1);
        %         y3=cells(i,2);
        %     end
        %     %
        %     vol_tri=abs(x1*y2+x3*y1+x2*y3-(x3*y2+x1*y3+x2*y1))/2;
        %     %
        %     % Pontos de Gauss %
        %     [x_gauss,ngauss]=gausspoints(x1,y1,x2,y2,x3,y3,'2D',8);
        %     %
        %     for k=1:ngauss                     
        %         if strcmp(solution,'paper')==1
        %         aux2=SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'lap');
        %         else
        %         aux2=gamma_diff*SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'lap')+u_convec_x*SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'xflux')+u_convec_y*SolutionDiffusion(solution,x_gauss(k,1),x_gauss(k,2),'yflux'); 
        %         end
        %         aux1=aux1+aux2*x_gauss(k,3)*vol_tri/cell_vol(i);
        %     end
        %     lap_phi(i)=lap_phi(i)+aux1;
        % end
    otherwise
        error('\n\nERRO: Método Não Implementado\n\n');
end

%
%% Faces de Fronteira %%
%
face_bool   = face_bound(:,1);
face_bool_y = face_bound(:,2);
face_bool_x = face_bound(:,3);
%

% Função Analitica %
phi_faces = zeros(face_num, 1);
phi_faces(face_bool) = SolutionDiffusion(solution, ...
            faces(face_bool,1),faces(face_bool,2),'anal');
%
% Calula o Fluxo de Phi com base nos Pontos de Gauss %
switch metodo
case {'FDM_2' 'WLS_2'}
    %
    % 2ª Ordem %
    flux_phi_faces = zeros(face_num, 2);
    flux_phi_faces(face_bool_x,1) = face_area(face_bool_x,2) .*   ...
                SolutionDiffusion(solution, faces(face_bool_x,1), ...
                faces(face_bool_x,2), 'xflux');
    flux_phi_faces(face_bool_y,2) = face_area(face_bool_y,3) .*   ...
                SolutionDiffusion(solution, faces(face_bool_y,1), ...
                faces(face_bool_y,2), 'yflux');
    %
case 'WLS_4' %% TODO %%
    %
    % 4ª Ordem %
    x1 = verts(face_vert(:,1),1);
    y1 = verts(face_vert(:,1),2);
    x2 = verts(face_vert(:,2),1);
    y2 = verts(face_vert(:,2),2);
    x3 = faces(:,1);
    y3 = faces(:,2);
    %
    % Pontos de Gauss %
    [G, ngauss] = gausspoints( x1, y1, x2, y2, x3, y3, '1D', 4);
    %
    aux_x = SolutionDiffusion( solution, G(:,1,:), G(:,2,:), 'xflux' ) .* G(:,3,:);
    aux_y = SolutionDiffusion( solution, G(:,1,:), G(:,2,:), 'yflux' ) .* G(:,3,:);
    %
    aux_x = sum( aux_x, 1);
    aux_y = sum( aux_y, 1);
    %
    flux_phi_faces = zeros(face_num,2);
    flux_phi_faces(:,1) = aux_x(:) .* face_area(:,2);
    flux_phi_faces(:,2) = aux_y(:) .* face_area(:,3);
    %
case 'WLS_6' %% TODO %%
    %
    % 6ª Ordem %
    % x1=verts(face_vert(i,1),1);
    % y1=verts(face_vert(i,1),2);
    % x2=verts(face_vert(i,2),1);
    % y2=verts(face_vert(i,2),2);
    % x3=faces(i,1);
    % y3=faces(i,2);
    % %
    % % Pontos de Gauss %
    % [x_gauss,ngauss]=gausspoints(x1,y1,x2,y2,x3,y3,'1D',6);
    % %
    % flux_phi_faces(i,1)=0;
    % flux_phi_faces(i,2)=0;
    % %
    % for j=1:ngauss
    %     aux_x=SolutionDiffusion(solution,x_gauss(j,1),x_gauss(j,2),'xflux')*x_gauss(j,3);
    %     aux_y=SolutionDiffusion(solution,x_gauss(j,1),x_gauss(j,2),'yflux')*x_gauss(j,3);
    %     %
    %     flux_phi_faces(i,1)=flux_phi_faces(i,1)+aux_x;
    %     flux_phi_faces(i,2)=flux_phi_faces(i,2)+aux_y;
    % end
case 'WLS_8' %% TODO %%
    %
    % 8ª Ordem %
    % x1=verts(face_vert(i,1),1);
    % y1=verts(face_vert(i,1),2);
    % x2=verts(face_vert(i,2),1);
    % y2=verts(face_vert(i,2),2);
    % x3=faces(i,1);
    % y3=faces(i,2);
    % %
    % % Pontos de Gauss %
    % [x_gauss,ngauss]=gausspoints(x1,y1,x2,y2,x3,y3,'1D',8);
    % %
    % flux_phi_faces(i,1)=0;
    % flux_phi_faces(i,2)=0;
    % %
    % for j=1:ngauss
    %     aux_x=SolutionDiffusion(solution,x_gauss(j,1),x_gauss(j,2),'xflux');
    %     aux_y=SolutionDiffusion(solution,x_gauss(j,1),x_gauss(j,2),'yflux');
    %     flux_phi_faces(i,1)=flux_phi_faces(i,1)+aux_x*face_area(i,2);
    %     flux_phi_faces(i,2)=flux_phi_faces(i,2)+aux_y*face_area(i,3);
    % end
otherwise
    error('\n\nERRO: Método Não Implementado\n\n');
end
