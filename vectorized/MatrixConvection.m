function [A,source,source_faces,source_cells]=MatrixConvection(dirichlet,order,T,G,stencil_cells,stencil_faces,stencil_size)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   26 de Setembro de 2016                                          %
%                                                                                                   %
% Função que implementa a Solução Numérica a partir do Minimos Quadrados de 2ª Ordem                %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


global cell_num face_num;
global faces;
global cell_faces cell_vol cell_norm;
global face_area;
global lap_phi phi_faces;
global u_convec_x u_convec_y;
%
if ~dirichlet
    error('\n\nERRO:Condição de Fronteira Não Implementada\n\n');
end
%
%% Normal exterior e coordenadas das faces
nx = cell_norm(:, 1:2:7);
ny = cell_norm(:, 2:2:8);
%
%% Pontos de Gauss
n = size(G,1);                          % numero de pontos de gauss
x = reshape( G(:,1,:),  [n face_num] )'; % coordenadas x
y = reshape( G(:,2,:),  [n face_num] )'; % coordenadas y
p = reshape( G(:,3,:),  [n face_num] )'; % pesos
%
%% Pre-alocar/inicializar outputs
A = spalloc(cell_num, cell_num, 9*cell_num);
source       = zeros(cell_num,1);
source_faces = zeros(cell_num,1);
source_cells = zeros(cell_num,1);
%
for i=1:cell_num
    %
    face = cell_faces(i,:);
    % Coordenadas das faces e pontos de Gauss
    xf = faces(face,1);
    yf = faces(face,2);
    xg = x(face,:);
    yg = y(face,:);
    %
    sz_1 = stencil_size(face(1),:);
    sz_2 = stencil_size(face(2),:);
    sz_3 = stencil_size(face(3),:);
    sz_4 = stencil_size(face(4),:);
    %
    % Vetores auxiliares - indices
    aux_cells=[ 1                            :  sz_1(2)                           , ...
                1 + sz_1(1)                  :  sz_2(2) + sz_1(1)                 , ...
                1 + sz_1(1)+sz_2(1)          :  sz_3(2) + sz_1(1)+sz_2(1)         , ...
                1 + sz_1(1)+sz_2(1)+sz_3(1)  :  sz_4(2) + sz_1(1)+sz_2(1)+sz_3(1) ];
    %
    ind_cells=[    ones(1,sz_1(2))  , ...
                2+zeros(1,sz_2(2)) , ...
                3+zeros(1,sz_3(2)) , ...
                4+zeros(1,sz_4(2)) ];
    
    %
    %
    % Vetores auxiliares - geometria
    aux_area = face_area(face,1);
    aux_nx = nx(i,:);
    aux_ny = ny(i,:);
    aux_area = aux_area(ind_cells);
    aux_nx = aux_nx(ind_cells)';
    aux_ny = aux_ny(ind_cells)';
    %
    % Constantes %
    C = cell2mat_mod( T(face), 1 );
    C_cells = C(aux_cells,:);
    %
    %
    %% CALCULAR POLINOMIOS PARA STENCIL DE CELULAS %%
    switch order
    %
    % 2ª Ordem %
    case 2
        [aux_poly, ~] = PolyReconstruction2ndOrder(C_cells, xg, yg, xf, yf, 'poly', face, ind_cells');
        %
        aux_x = u_convec_x .* aux_nx .* aux_area .* aux_poly;
        aux_y = u_convec_y .* aux_ny .* aux_area .* aux_poly;
        %
    %
    % 4ª Ordem %
    case 4
        [aux_poly, ~] = PolyReconstruction4thOrder(C_cells, xg, yg, xf, yf, 'poly', face, ind_cells');
        %
        aux_poly = sum( aux_poly.*p(ind_cells,:), 2);
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    %
    % 6ª Ordem %
    case 6
        [aux_poly, ~] = PolyReconstruction6thOrder(C_cells, xg, yg, xf, yf, 'poly', face, ind_cells');
        %
        aux_poly = sum( aux_poly.*p(ind_cells,:), 2);
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    %
    % 8ª Ordem
    case 8
        [aux_poly, ~] = PolyReconstruction8thOrder(C_cells, xg, yg, xf, yf, 'poly', face, ind_cells');
        %
        aux_poly = sum( aux_poly.*p(ind_cells,:), 2);
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    otherwise
        error('\n\nERRO: Ordem Não Implementada\n\n');
    end
    %
    % Construir fileira 'i' da matrix A
    aux_cells = nonzeros(stencil_cells(face,:)');   % celulas dos stencils das faces da celula 'i'
    aux_uniqs = unique(aux_cells);                  % ignorar celulas repetidas
    aux_bool = ( aux_cells' == aux_uniqs );         % matriz bool indicando contribuicoes repetidas 
    aux_xy = aux_x + aux_y;
    I = min(aux_uniqs);
    J = max(aux_uniqs);
    aux_A = zeros(J-I+1, 1);
    for j=1:length(aux_uniqs)
        aux_A(aux_uniqs(j)-I+1) = sum(aux_xy(aux_bool(j,:)));
    end
    A(I:J, i) = aux_A;    
    %
    %
    if ( sz_1(3)+sz_2(3)+sz_3(3)+sz_4(3)==0 )
        continue 
    end
    %
    % Vetores auxiliares - indices
    aux_faces=[ 1 + sz_1(2)                            :  sz_1(1)                         , ...
                1 + sz_2(2) + sz_1(1)                  :  sz_1(1)+sz_2(1)                 , ...
                1 + sz_3(2) + sz_1(1)+sz_2(1)          :  sz_1(1)+sz_2(1)+sz_3(1)         , ...
                1 + sz_4(2) + sz_1(1)+sz_2(1)+sz_3(1)  :  sz_1(1)+sz_2(1)+sz_3(1)+sz_4(1) ];
    %
    ind_faces=[    ones(1,sz_1(3)) , ...
                2+zeros(1,sz_2(3)) , ...
                3+zeros(1,sz_3(3)) , ...
                4+zeros(1,sz_4(3)) ];
    %
    % Vetores auxiliares - geometria
    aux_area = face_area(face,1);
    aux_nx = nx(i,:);
    aux_ny = ny(i,:);
    aux_area = aux_area(ind_faces);
    aux_nx = aux_nx(ind_faces)';
    aux_ny = aux_ny(ind_faces)';
    %
    % Constantes %
    C_faces = C(aux_faces,:);
    %
    %
    %% CALCULAR POLINOMIOS PARA STENCIL DE FACES %%
    switch order
    %
    % 2ª Ordem %
    case 2
        [aux_poly, ~] = PolyReconstruction2ndOrder(C_faces, xg, yg, xf, yf, 'poly', face, ind_faces');
        %
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    %
    % 4ª Ordem %
    case 4
        [aux_poly, ~] = PolyReconstruction4thOrder(C_faces, xg, yg, xf, yf, 'poly', face, ind_faces');
        %
        aux_poly = sum( aux_poly.*p(ind_faces,:), 2);
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    %
    % 6ª Ordem %
    case 6
        [aux_poly, ~] = PolyReconstruction6thOrder(C_faces, xg, yg, xf, yf, 'poly', face, ind_faces');
        %
        aux_poly = sum( aux_poly.*p(ind_faces,:), 2);
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    %
    % 8ª Ordem
    case 8
        [aux_poly, ~] = PolyReconstruction8thOrder(C_faces, xg, yg, xf, yf, 'poly', face, ind_faces');
        %
        aux_poly = sum( aux_poly.*p(ind_faces,:), 2);
        aux_x = u_convec_x .* aux_poly .* aux_area .* aux_nx;
        aux_y = u_convec_y .* aux_poly .* aux_area .* aux_ny;
        %
    otherwise
        error('\n\nERRO: Ordem Não Implementada\n\n');
    end
    %
    % Calcular termos de fonte das faces da celula 'i'
    aux_faces = nonzeros(stencil_faces(face,:)); % faces dos stencils das faces da celula 'i'
    source_faces(i) = sum( (aux_x + aux_y) .* phi_faces(aux_faces) );
end
%
A = A';
%
% Finalizar calculo dos termos de fonte
source_cells = lap_phi .* cell_vol;
source = - source_faces;
%
%
fprintf(' ... Fim\n');
%